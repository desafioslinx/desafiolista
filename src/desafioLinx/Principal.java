package desafioLinx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Principal {

	public static final List<Integer> NUMEROS_MULTIPLO_DE_3 = new ArrayList<>(Arrays.asList(3,6,9));
	private int vetorCalculado[] = new int[10];
	
	public final int TAMANHO_PADRAO_VETOR = vetorCalculado.length;
	
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		int valorMultiploDeDez =  entrada.nextInt();
		int resultadoCalculado = 0;
		Principal p = new Principal();
		
		p.armazenarPosicoesNaoMultiplosDeTres(valorMultiploDeDez);
		
		p.armazenarPosicoesMultiplasDeTres(valorMultiploDeDez);

		System.out.println("1 - Pares\n2 - �mpares");
		
		int opcaoEscolhidaPeloUsuario = entrada.nextInt();
		
		resultadoCalculado = Principal.calcularSomaDasPosicoesImparesOuPares(p.getVetorCalculado(), opcaoEscolhidaPeloUsuario == 1);
		
		System.out.println("Resultado: "+resultadoCalculado);
		entrada.close();
	}

	
	public int[] getVetorCalculado() {
		return vetorCalculado;
	}


	public void setVetorCalculado(int[] vetorCalculado) {
		this.vetorCalculado = vetorCalculado;
	}
	
	public static int calcularSomaDasPosicoesImparesOuPares(int valoresParaCalcular[],boolean isPar) {
		return IntStream.range(0, valoresParaCalcular.length)
		.filter(vetor -> (isPar ? ((vetor+1) % 2 == 0) : (vetor+1) % 2 != 0) )
		.reduce(0,(resultado,vetor) -> valoresParaCalcular[vetor] + resultado);
	}
	
	
	public void armazenarPosicoesNaoMultiplosDeTres(int entradaDoUsuario) {
		IntStream
		.range(0, TAMANHO_PADRAO_VETOR)
		.filter(indicePosicao -> !NUMEROS_MULTIPLO_DE_3.contains(indicePosicao+1))
		.forEach(indicePosicao -> {
			vetorCalculado[indicePosicao] = calcularPosicao(indicePosicao, entradaDoUsuario);
		});
	}
	
	public void armazenarPosicoesMultiplasDeTres(int entradaDoUsuario) {
		IntStream.range(0, TAMANHO_PADRAO_VETOR).filter(indicePosicao -> {
			return NUMEROS_MULTIPLO_DE_3.contains(indicePosicao + 1);
		})
		.forEach(indicePosicao -> {
			vetorCalculado[indicePosicao] = new Double(Math.ceil((indicePosicao+1) * 0.3 * entradaDoUsuario)).intValue();
		});
	}
	
	public int calcularPosicao(int indicePosicao, int entradaDoUsuario) {
		return new Double(Math.ceil((indicePosicao+1) * 0.1 * entradaDoUsuario)).intValue();
	}
}
