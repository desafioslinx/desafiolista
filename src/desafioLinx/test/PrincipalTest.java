package desafioLinx.test;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import desafioLinx.Principal;

public class PrincipalTest {

	private final Principal prin = new Principal();
	
	@Test
	public void deveCalcularSomaDosValoresImparesOuPares() {
		Assertions.assertEquals(200, Principal.calcularSomaDasPosicoesImparesOuPares(new int[]{20,40,180}, false));
	}
	
}
